# Front Project

* This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.
* This project is used to help testing rest api in java-test/branch02. [Link](https://gitlab.com/qintu/java-test/commits/branch02)
* CSS and some layouts are missing in this project.


## How to run it
* First make sure nodejs/npm/angular 4+ are already installed.
* use `npm install` to install all dependencies.
* use `npm start` to start it.
* type url "http://localhost:4200" in your brower, you will see the simple tests for java-test/branch02.

**Thank you!!!** 
 
