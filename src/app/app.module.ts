import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ShowUsersComponent } from './show-users/show-users.component';
import { HomeComponent } from './home/home.component';
import { UserService } from './user.service';
import { AppService } from './app.service';
import { UserManagermentComponent } from './user-managerment/user-managerment.component';
import { UserResolveService } from './user-resolve.service';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'users', component: ShowUsersComponent, resolve: {users: UserResolveService} },
  { path: 'user', component: UserManagermentComponent},
  { path: '**', component: HomeComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ShowUsersComponent,
    HomeComponent,
    UserManagermentComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AppService,
    UserService,
    UserResolveService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [AppService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
function ConfigLoader(config: AppService) {
  return () => config.load();
}
