import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-users',
  templateUrl: './show-users.component.html',
  styleUrls: ['./show-users.component.css']
})
export class ShowUsersComponent implements OnInit {
  users: any[];
  constructor(private route: ActivatedRoute) { }
  ngOnInit() {
    this.users = this.route.snapshot.data['users'];
    console.log(this.users);
  }

}
