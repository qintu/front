import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder, ValidatorFn } from '@angular/forms';
import { UserService } from '../user.service';
import { User } from '../model/User';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-user-managerment',
  templateUrl: './user-managerment.component.html',
  styleUrls: ['./user-managerment.component.css']
})


export class UserManagermentComponent implements OnInit {
  status;
  user: any;
  isShow: boolean;
  email: string;
  name: string;
  r: string;
  roles = [
    { name: 'PM', id: 'PM' },
    { name: 'PO', id: 'PO' },
    { name: 'QA', id: 'QA' }
  ];
  formdata: FormGroup;
  searchform: FormGroup;
  controls;

  constructor(private userService: UserService, private formBuilder: FormBuilder, private route: ActivatedRoute,
    private router: Router) {

    this.searchform = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[^ @]*@[^ @]*')
      ]))
    });

    this.controls = this.roles.map(c => new FormControl(false));
    console.log(this.controls);
    this.controls[0].setValue(true);

    this.formdata = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[^ @]*@[^ @]*')
      ])),
      name: new FormControl(''),
      roles: new FormArray(this.controls, this.minSelectedCheckboxes(1))
    });
  }
  ngOnInit() {
    this.route.params.subscribe(d => this.status = d.status);
    this.isShow = this.status === 'update' || this.status === 'delete';
  }

  search() {
    this.email = this.searchform.value.email;
    if (this.isShow && this.email != null) {
      this.userService.getUserByEmail(this.email).subscribe(data => {
        if (data != null) {
          this.r = data['role'];
          this.name = data['name'];
          for (let i = 0; i < this.controls.length; i++) {
            if (data['role'] != null) {
              this.controls[i].setValue(data['role'][i]);
            } else {
              this.controls[i].setValue(false);
            }
          }
          this.formdata.controls['email'].setValue(data['email']);
          this.formdata.controls['name'].setValue(data['name']);
        } else {
          alert('email is not existing, please input email again!!!');
        }
      });
    }
    switch (this.status) {
      case 'update': {
        this.formdata.controls['email'].disable();
        break;
      }
      case 'delete': {
        this.formdata.controls['name'].disable();
        this.formdata.controls['email'].disable();
        this.formdata.controls['roles'].disable();
        break;
      }
    }
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);

      return totalSelected >= min ? null : { required: true };
    };
    return validator;
  }

  submit() {
    const u = new User();
    const selectedRoles = this.formdata.value.roles
      .map((v, i) => v ? this.roles[i].id : null)
      .filter(v => v !== null);
    u.email = this.formdata.value.email || this.email;
    u.role = selectedRoles || this.r;
    u.name = this.formdata.value.name || this.name;
    console.log(u);
    switch (this.status) {
      case 'update': {
        console.log('update');
        this.userService.updateUserByEmail(u.email, u).subscribe(data => {
          this.router.navigate(['/users']);
        }, err => {
          this.router.navigate(['/home']);
        });
        break;
      }
      case 'delete': {
        this.userService.deleteUserByEmail(u.email).subscribe(data => {
          this.router.navigate(['/users']);
        }, err => {
          this.router.navigate(['/home']);
        });
        break;
      }
      case 'add': {
        this.userService.addUser(u).subscribe(data => {
          this.router.navigate(['/users']);
        }, err => {
          this.router.navigate(['/home']);
        });
        break;
      }
    }
  }



}
