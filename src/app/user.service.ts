import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as URI_ from 'urijs';
import { AppService } from './app.service';
const URI = URI_;

@Injectable()
export class UserService {

  baseURL = URI(this.config.get('baseURL'));
  constructor(private http: HttpClient, private config: AppService) { }

  getAllUsers() {
    return this.http.get<any>(URI(this.baseURL).segment('users/').toString());
  }
  getUserByEmail(email: string) {
    return this.http.get(URI(this.baseURL).segment('users/').segment(email).toString());
  }
  deleteUserByEmail(email: string) {
    return this.http.delete(URI(this.baseURL).segment('users/').segment(email).toString());
  }
  updateUserByEmail(email: string, user: any) {
    return this.http.put(URI(this.baseURL).segment('users/').segment(email).toString(), user);
  }
  addUser(user: any) {
    return this.http.post(URI(this.baseURL).segment('users/').toString(), user);
  }


}
