import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class AppService {
  private config: any;
  constructor() { }
  load() {
    return fetch(`assets/config/config-${environment.name}.json`)
      .then(v => v.json()).then(v => this.config = v);
  }

  get(key: string) {
    return this.config[key];
  }
  getEnv() {
    return environment.name;
  }
}
