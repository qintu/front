import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { UserService } from './user.service';


@Injectable()
export class UserResolveService implements Resolve<any> {
  constructor(private userService: UserService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.userService.getAllUsers().pipe(map(
      res => res),
      catchError(err => err)
    );
  }

}
